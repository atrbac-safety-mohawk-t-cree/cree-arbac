package cree;

import java.util.logging.Logger;

public class CreeSolver {
    public static final Logger logger = Logger.getLogger("mohawk");

    public static void main(String[] args) {
        CreeSolverInstance inst = new CreeSolverInstance();

        inst.run(args);
    }

}
