package cree.helper;

import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import cree.optimization.heuristics.MohawkTHeuristics;
import cree.optimization.slicing.StaticSlicer;
import cree.results.StaticSlicerResults;
import mohawk.global.convert.nusmv.ConvertToNuSMV;
import mohawk.global.helper.FileHelper;
import mohawk.global.pieces.MohawkT;
import mohawk.global.pieces.mohawk.NuSMVMode;
import mohawk.global.results.*;
import mohawk.global.timing.MohawkTiming;

/** @author Jonathan Shahen */
public class CreeTestingSuite {
    public final static Logger logger = Logger.getLogger("mohawk");

    public MohawkResults results;
    public MohawkTiming timing;
    public StaticSlicerResults slicerResults;

    public MohawkT lastTestCase;
    public ExecutionResult lastResult;
    public CounterExample lastCounterExample;

    public String testingResultsFile = "logs/mohawkTestingResults.csv";
    public String slicingResultsFile = "logs/MohawkT-StaticSlicer-Results.csv";
    /** The file-path for the SMV file output. <br/>
     * <b>NOTE:</b> The extension ".smv" will be added to this file. */
    public String smvFilepath = "logs/latestSMVFile";
    public String programPath;

    public NuSMVMode mode = NuSMVMode.SMC;

    public long TIMEOUT_SECONDS = 0;

    /** Flag to turn on DEBUGGING mode ON or OFF. <br/>
     * When ON extra output files will be generated to show more information about the steps that
     * were taken to come to the final result. */
    public boolean debug = false;

    public boolean printOutResult = true;

    public CreeTestingSuite(FileHelper fileHelper, MohawkResults results, StaticSlicerResults slicerResults,
            MohawkTiming timing, String programPath) {
        this.results = results;
        this.slicerResults = slicerResults;
        this.timing = timing;
        this.programPath = programPath;
    }

    /** If you only want to convert SPEC files to SMV files. This can be used before running if there happens to be a
     * crash while running the tests you can just resume from the test and not have to convert the SPEC to SMV every
     * time.
     * 
     * @param m */
    public void convertToSMVFormat(MohawkT m) {
        logger.entering(getClass().getName(), "convertToSMVFormat()");

        ConvertToNuSMV convert = new ConvertToNuSMV(timing);

        convert.convert(m, new File(smvFilepath), true);

        if (convert.lastError != null) {
            logger.severe("Unable to convert Mohawk+T input policy to NuSMV!\nMohawk+T Input policy:" + m);
        }

        logger.exiting(getClass().getName(), "convertToSMVFormat()");
    }

    /** This will take care of fully running the test, if it needs to convert SPEC files it will and while it runs it
     * will write out results after each SMV file is run (just in case of a crash).
     * 
     * @param testcase Input Policy to test and return an result
     * @param origTimerName a way to create unique timer names within this function (because it is assumed to be called
     *            with more than once with different policies)
     * @param inputPolicyFilepath The filepath to the Mohawk+T Policy file that is being read in
     * @param writeOutResult When TRUE, the function will write and print out the results at the end of the function;
     *            when FALSE it will only print
     * @param abstractionRefinement When TRUE, the test will perform abstraction refinement on {@code testcase};
     *            otherwise the policy {@code testcase} will be converted to SMV and run in the model checker.
     * @param staticSlicing 
     * @param skipRunning When TRUE, the test will convert but not run the model checker
     * @return
     * @throws IOException */
    public ExecutionResult runTest(MohawkT testcase, String origTimerName, String inputPolicyFilepath,
            boolean writeOutResult, boolean abstractionRefinement, boolean staticSlicing, boolean skipRunning)
            throws IOException {
        logger.entering(getClass().getName(), "runTest()");

        FileWriter resultsFW = null;
        if (writeOutResult) {
            resultsFW = results.getFileWriter(new File(testingResultsFile), false);
            if (resultsFW == null) {
                logger.severe("[runTests] Unable to Create, or Write in, the results file: " + testingResultsFile);
                throw new IOException("Unable to Create, or Write in, the results file: " + testingResultsFile);
            }
        }
        FileWriter slicerFW = null;
        if (staticSlicing) {
            slicerFW = slicerResults.getFileWriter(new File(slicingResultsFile), false);
            if (slicerFW == null) {
                logger.severe(
                        "[runTests] Unable to Create, or Write in, the static slicing file: " + slicingResultsFile);
                throw new IOException("Unable to Create, or Write in, the static slicing file: " + slicingResultsFile);
            }
        }

        ArrayList<NuSMVMode> modes = new ArrayList<NuSMVMode>();
        switch (mode) {
            case BOTH :// Both
                modes.add(NuSMVMode.SMC);
                modes.add(NuSMVMode.BMC);
                break;
            case BMC :// BMC
                modes.add(NuSMVMode.BMC);
                break;
            case SMC :// SMC
                modes.add(NuSMVMode.SMC);
                break;
            default :
                logger.severe("[ERROR] Unknown mode = " + mode);
        }

        ExecutorService executor = Executors.newSingleThreadExecutor();

        /////////////////////////////////////////////////////////////////////
        // STATIC SLICING
        if (staticSlicing) {
            /* Timing */timing.startTimer(origTimerName + "-StaticSlicing");
            StaticSlicer slicer = new StaticSlicer(testcase, inputPolicyFilepath, timing, origTimerName);
            slicer.DEBUG = debug;
            if (debug == true) {
                slicer.writeOutNewPolicy = true;
                slicer.newPolicyFilename = slicer.origPolicyFilename + ".sliced";
            }
            slicer.slicePolicy();
            testcase = slicer.getSlicedPolicy();
            /* Timing */timing.stopTimer(origTimerName + "-StaticSlicing");

            /* Timing */timing.startTimer(origTimerName + "-writeOutSlicerResults");
            slicerResults.add(slicer.slicerResult);
            slicerResults.writeOutLast(slicerFW);
            /* Timing */timing.stopTimer(origTimerName + "-writeOutSlicerResults");

            logger.info("[Static Slicing] Slincing Results: " + slicer.slicerResult);
        }
        // STATIC SLICING
        /////////////////////////////////////////////////////////////////////

        lastTestCase = testcase;

        /////////////////////////////////////////////////////////////////////
        // FOR LOOP: modes of operation
        for (Integer j = 0; j < modes.size(); j++) {
            /* TIMING */String timerName = origTimerName + "(Mode=" + modes.get(j) + ")";
            /* TIMING */timing.startTimer(timerName);

            logger.info("Working on " + timerName + " on file: " + inputPolicyFilepath);

            /////////////////////////////////////////////////////////////////////
            // START HEURISTICS
            /* TIMING */timing.startTimer(timerName + "-heuristics");
            MohawkTHeuristics heuristics = new MohawkTHeuristics(testcase);
            heuristics.debug = debug;
            ExecutionResult result = heuristics.getResult();
            /* TIMING */timing.stopTimer(timerName + "-heuristics");
            // END HEURISTICS
            /////////////////////////////////////////////////////////////////////

            if (result != ExecutionResult.GOAL_UNKNOWN) {
                lastResult = result;
                lastCounterExample = new CounterExample(result == ExecutionResult.GOAL_REACHABLE);
                /* TIMING */timing.stopTimer(timerName);

                // Store and print out the results
                results.add(new TestingResult(lastResult, // The ExecutionResult
                        testcase.expectedResult.toExecutionResult(), // The expected result from the testcase
                        timing.getLastElapsedTime(), // Duration
                        inputPolicyFilepath, // File of the Input Policy
                        "Mohawk+T_" + programPath,// Program
                        modes.get(j).toString(), // SMC or BMC mode
                        abstractionRefinement, //
                        staticSlicing, //
                        "Not Setup Yet!",// Return Code from
                        "Heuristics Made Decision",// Comment
                        lastCounterExample));
                if (writeOutResult) {
                    results.writeOutLast(resultsFW);
                }
            }

            // Test runner
            CreeTestRunner testRunner = new CreeTestRunner(testcase, programPath, inputPolicyFilepath,
                    new File(smvFilepath), modes.get(j), timerName, timing, abstractionRefinement, skipRunning);

            testRunner.debug = debug;

            // Setup Timeout Timer
            Future<ExecutionResult> future = executor.submit(testRunner);
            try {
                logger.info("[RUNNING] Running with Mode: " + modes.get(j));

                if (TIMEOUT_SECONDS == 0) {
                    lastResult = future.get();
                } else {
                    lastResult = future.get(TIMEOUT_SECONDS, TimeUnit.SECONDS);
                }
                /* TIMING */timing.stopTimer(timerName);

                // Store and print out the results
                if (lastResult == null) {
                    logger.severe("ERROR: MohawkTTestRunner returned NULL instead of a ExectuionResult");
                    lastResult = ExecutionResult.ERROR_OCCURRED;
                    results.add(new TestingResult(lastResult, //
                            testcase.expectedResult.toExecutionResult(), // The expected result from the testcase
                            timing.getLastElapsedTime(),// Duration
                            inputPolicyFilepath,// Input Mohawk+T Filename
                            "Mohawk+T_" + programPath,// Program
                            modes.get(j).toString(),// Mode (BMC or SMC)
                            abstractionRefinement,//
                            staticSlicing,//
                            String.valueOf(testRunner.exitValue),// Executable return code
                            "ERROR: MohawkTTestRunner returned NULL instead of a ExectuionResult",
                            testRunner.lastCounterExample));
                } else {
                    lastCounterExample = testRunner.lastCounterExample;
                    lastCounterExample.conditionalRemoveLastStep();
                    results.add(new TestingResult(lastResult, //
                            testcase.expectedResult.toExecutionResult(), // The expected result from the testcase
                            timing.getLastElapsedTime(), // Duration
                            inputPolicyFilepath,// Input Mohawk+T Filename
                            "Mohawk+T_" + programPath,// Program
                            modes.get(j).toString(), // Mode (BMC or SMC)
                            abstractionRefinement,//
                            staticSlicing,//
                            String.valueOf(testRunner.exitValue),// Executable return code
                            "",// Comment
                            testRunner.lastCounterExample));
                }

                if (writeOutResult) {
                    results.writeOutLast(resultsFW);
                }
                logger.info("[Result] For Testcase: " + inputPolicyFilepath + " (constructed from Mohawk+T policy: "
                        + testcase.policyFilename + ") running in mode: " + modes.get(j).toString()
                        + ", the result is: " + results.getLastResult());

                if (printOutResult) {
                    System.out.println("[Result] For Testcase: " + inputPolicyFilepath
                            + " (constructed from Mohawk+T policy: " + testcase.policyFilename + ") running in mode: "
                            + modes.get(j).toString() + ", the result is: " + results.getLastResult());
                }
            } catch (TimeoutException e) {
                logger.warning("[TIMEOUT] Mohawk has timed out for SPEC file: " + inputPolicyFilepath);

                lastResult = ExecutionResult.TIMEOUT;
                testRunner.kill();

                /* TIMING */timing.cancelTimer(timerName);
            } catch (OutOfMemoryError e) {
                logger.warning(
                        "[OUT OF MEMORY] Mohawk has ran out of memory out for SPEC file: " + inputPolicyFilepath);

                lastResult = ExecutionResult.OUT_OF_MEMORY;

                /* TIMING */timing.cancelTimer(timerName);
            } catch (InterruptedException e) {
                if (logger.getLevel() == Level.FINEST) {
                    e.printStackTrace();
                }
                /* TIMING */timing.cancelTimer(timerName);
                logger.severe(e.getMessage());
            } catch (ExecutionException e) {
                StringWriter errors = new StringWriter();
                e.printStackTrace(new PrintWriter(errors));
                logger.severe(errors.toString());
                logger.severe(e.getMessage());
                /* TIMING */timing.cancelTimer(timerName);
            }

            logger.info("[COMPLETED] Result (Mode: " + modes.get(j) + "): " + lastResult + ", Elapsed Time: "
                    + timing.getLastElapsedTimeSec() + " secs; for the Query: '" + testcase.query + "'");
        }
        // FOR LOOP: modes of operation
        /////////////////////////////////////////////////////////////////////
        executor.shutdown();

        if (writeOutResult) {
            resultsFW.close();
        }

        if (staticSlicing) {
            slicerFW.close();
        }

        logger.exiting(getClass().getName(), "runTest()");
        return lastResult;
    }
}
