package cree.results;

import java.io.File;
import java.time.LocalDateTime;

public class DiameterResult {
    /** The value that determines if an exponent is too large to be a viable diameter */
    public static int EXPONENT_TOO_LARGE = 200;

    /** The name of the file that was passed to the solver */
    public String _filename;
    /** Any comments that should be attached to this testing result */
    public String _comment;
    /** the date and time that the testing result was created */
    public String _datetime;

    public Integer _numRules;
    public Integer _numRoles;
    public Integer _numTimeslots;
    public Integer _numAdminRoles;

    /** This is the exponent above the 2 == 2^{_diameterInitial} */
    public Integer _diameterInitial;
    /** This is the exponent above the 2 == 2^{_diameterTightening01} */
    public Integer _diameterTightening01;
    /** This is the exponent above the 2 == 2^{_diameterTightening02} */
    public Integer _diameterTightening02;
    /** This is the exponent above the 2 == 2^{_diameterTightening03} */
    public Integer _diameterTightening03;

    public String _diameterInitialStr;
    public String _diameterTightening01Str;
    public String _diameterTightening02Str;
    public String _diameterTightening03Str;

    public Long _timingTightening01;
    public Long _timingTightening02;
    public Long _timingTightening03;
    public Long _totalTiming;

    public DiameterResult(String filename, String comment) {
        _filename = filename;
        _comment = comment;
        _datetime = LocalDateTime.now().toString();
    }

    public DiameterResult(File specFile, String comment) {
        this(specFile.getAbsolutePath(), comment);
    }

    /** Checks to see if the exponent value is too large to do 2^exponent
     * 
     * @param exponent
     * @return TRUE if the value in {@code exponent} is too large */
    public static boolean isTooLarge(double exponent) {
        if (exponent >= EXPONENT_TOO_LARGE) return true;
        return false;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();

        s.append("TestingResult{").append(_filename).append("; Date: ").append(_datetime)//
                .append("; |Rules|: ").append(_numRules)//
                .append("; |Roles|: ").append(_numRoles)//
                .append("; |Timeslots|: ").append(_numTimeslots)//
                .append("; |Admin Roles|: ").append(_numAdminRoles)//
                .append("; Initial Diameter: ").append(_diameterInitialStr).append(" = ").append(_diameterInitial)//
                .append("; Final Duration: ").append(_totalTiming).append("ms ")//
                .append("; Diameter: [")//
                .append(_diameterTightening01Str).append(" = ").append(_diameterTightening01).append(", ")//
                .append(_diameterTightening02Str).append(" = ").append(_diameterTightening02).append(", ")//
                .append(_diameterTightening03Str).append(" = ").append(_diameterTightening03).append(", ")//
                .append("]")//
                .append("; Duration: [").append(_timingTightening01).append("ms, ").append(_timingTightening02)
                .append("ms, ").append(_timingTightening03).append("ms]")//
                .append("; Comment: ").append(_comment).append("}");

        return s.toString();
    }

    /** Returns the latest diameter that was produced. If none are found then it will return 0
     * 
     * @return */
    public double getDiameter() {
        if (_diameterTightening03 != null) { return _diameterTightening03; }
        if (_diameterTightening02 != null) { return _diameterTightening02; }
        if (_diameterTightening01 != null) { return _diameterTightening01; }
        if (_diameterInitial != null) { return _diameterInitial; }
        return 0;
    }
}
