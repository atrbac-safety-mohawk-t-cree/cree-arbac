/** Quick functions to tell the result of a Mohawk+T Policy without running it through a solver.
 * 
 * @author Jonathan Shahen */
package cree.optimization.heuristics;
