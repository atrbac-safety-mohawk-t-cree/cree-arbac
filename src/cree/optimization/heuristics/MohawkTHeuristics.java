package cree.optimization.heuristics;

import java.util.*;
import java.util.logging.Logger;

import mohawk.global.pieces.*;
import mohawk.global.results.ExecutionResult;

public class MohawkTHeuristics {
    public final static Logger logger = Logger.getLogger("mohawk");

    public MohawkT m;
    /**  */
    public boolean debug = false;
    public String reason = null;

    public MohawkTHeuristics(MohawkT policy) {
        m = policy;
    }

    /** Check the Mohawk+T policy with certain heuristics to determine the result without running a solver
     * 
     * @return null if no result could be determined, REACHABLE/UNREACHABLE if guaranteed result */
    public ExecutionResult getResult() {
        ExecutionResult result = null;

        result = emptyQuery();
        if (result != null) { return result; }

        result = lessCARulesThanQueryRoles();
        if (result != null) { return result; }

        result = noCARulesForQueryRoles();
        if (result != null) { return result; }

        return ExecutionResult.GOAL_UNKNOWN;
    }

    /** If the Query role array is empty then every user satisfies the query and thus the policy is reachable
     * 
     * @return */
    public ExecutionResult emptyQuery() {
        ExecutionResult result = null;

        if (m.query._roles.isEmpty()) {
            result = ExecutionResult.GOAL_REACHABLE;
            logger.info("[HEURISTICS] Setting result = GOAL_REACHABLE because the query has no goal roles.");
            reason = "[emptyQuery] Result = GOAL_REACHABLE; the query has no goal roles.";
        }

        return result;
    }

    /** If the number of CanAssign rules is less than the number of roles in the Query,
     * then it is impossible to have a reachable state (assumed every user is assigned to 0 roles in the beginning)
     * 
     * @return */
    public ExecutionResult lessCARulesThanQueryRoles() {
        ExecutionResult result = null;

        if (m.canAssign.size() < m.query._roles.size()) {
            result = ExecutionResult.GOAL_UNREACHABLE;
            logger.info("[HEURISTICS] Setting result = GOAL_UNREACHABLE because there are less CanAssign rules than "
                    + "goal roles in the query.");
            reason = "[lessCARulesThanQueryRoles] Result = GOAL_UNREACHABLE; there are less CanAssign rules "
                    + "than goal roles in the query.";
        }

        return result;
    }

    public ExecutionResult noCARulesForQueryRoles() {
        ExecutionResult result = null;

        TimeSlot goalTimeslot = m.query._timeslot;
        Set<Role> goalRoles = new HashSet<Role>(m.query._roles);
        HashMap<Role, Character> found = new HashMap<Role, Character>(goalRoles.size());
        ArrayList<Rule> truelyRules = new ArrayList<>(goalRoles.size());

        for (Role r : goalRoles) {
            found.put(r, 'N');
        }

        for (Rule rule : m.canAssign.getRules()) {
            if (goalRoles.contains(rule._role)) {
                if (found.get(rule._role) != 'T') {
                    for (TimeSlot ts : rule._roleSchedule) {
                        if (ts.equals(goalTimeslot)) {
                            if (rule.checkIsRuleTrulyStartable()) {
                                found.put(rule._role, 'T');
                                truelyRules.add(rule);
                            } else {
                                found.put(rule._role, 'F');
                            }
                        }
                    }
                }
            }
        }

        boolean allFound = true;
        boolean truelyFound = true;
        for (Role r : goalRoles) {
            allFound = allFound & found.get(r) == 'N' ? false : true;
            truelyFound = truelyFound & found.get(r) == 'T' ? true : false;
        }

        if (truelyFound) {
            result = ExecutionResult.GOAL_REACHABLE;
            logger.info("[HEURISTICS] Setting result = GOAL_REACHABLE because there exists a Truely Startable "
                    + "CanAssign rule for each goal role within the timeslot " + goalTimeslot + ". "
                    + "Truely Startable rules required: " + truelyRules);
            reason = "[truelyCARulesForQueryRoles] Result = GOAL_REACHABLE; there exists a Truely Startable "
                    + "CanAssign rule for each goal role within the timeslot " + goalTimeslot + ". "
                    + "Truely Startable rules required: " + truelyRules;
        } else if (!allFound) {
            result = ExecutionResult.GOAL_UNREACHABLE;
            logger.info("[HEURISTICS] Setting result = GOAL_UNREACHABLE because there does not exist a "
                    + "CanAssign rule for each goal role within the timeslot " + goalTimeslot + ". Found CA Rules: "
                    + found);
            reason = "[noCARulesForQueryRoles] Result = GOAL_UNREACHABLE; there does not exist a "
                    + "CanAssign rule for each goal role within the timeslot " + goalTimeslot + ".";
        }

        return result;
    }
}
