package cree.optimization.boundestimation;

import java.util.*;
import java.util.logging.Logger;

import cree.results.DiameterResult;
import mohawk.global.helper.RoleHelper;
import mohawk.global.helper.TimeIntervalHelper;
import mohawk.global.pieces.*;
import mohawk.global.timing.MohawkTiming;

/** Class to estimate the bound for Mohawk+T Polices
 * 
 * @author Jonathan Shahen */
public class BoundEstimation {
    public static final Logger logger = Logger.getLogger("mohawk");
    public boolean DEBUG = false;

    public boolean runTightening01 = false;
    public boolean runTightening02 = false;
    public boolean runTightening03 = true;

    public MohawkTiming timing;
    public String timerKey;
    public DiameterResult diaResult;
    public MohawkT m;

    public Set<Role> adminRoles = null;
    public Set<Role> goalRoles = null;

    public ArrayList<Rule> CARules = null;
    public ArrayList<Rule> CRRules = null;
    public ArrayList<Rule> CERules = null;
    public ArrayList<Rule> CDRules = null;

    public Set<Role> CATargetRoles = null;
    public Set<Role> CACRPositivePrecondition = null;

    public Set<Role> CETargetRoles = null;
    public Set<Role> CECDPositivePrecondition = null;

    public BoundEstimation(MohawkT policy, String inputFilename, MohawkTiming timing, String timerKey) {
        m = policy;
        diaResult = new DiameterResult(inputFilename, "");
        this.timing = timing;
        this.timerKey = timerKey + "-BoundEst:";
    }

    /** Calculate the diameter that the model checker needs in Bounded Model Checking Mode
     * 
     * @return
     *         <ul>
     *         <li><b>0</b> an error occurred and no diameter was calculated
     *         <li><b>-value</b> when the exponent is too large it returns the negative exponent value
     *         <li><b>+value</b> the upper bound on the diameter
     *         </ul>
    */
    public double calculateBound() {
        String timerStr = timerKey + "calculateBound";
        /* TIMING */timing.startTimer(timerStr);

        gatherSimpleMetrics();

        ///////////////////////////////////////////////////////////////
        // TIGHTENING 01
        if (runTightening01) {
            /* TIMING */timing.startTimer(timerStr + "-tightening01");
            tightening01();
            /* TIMING */timing.stopTimer(timerStr + "-tightening01");
            diaResult._timingTightening01 = timing.getLastElapsedTime();
        }
        // end of TIGHTENING 01
        ///////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////
        // TIGHTENING 02
        if (runTightening02) {
            /* TIMING */timing.startTimer(timerStr + "-tightening02");
            tightening02();
            /* TIMING */timing.stopTimer(timerStr + "-tightening02");
            diaResult._timingTightening02 = timing.getLastElapsedTime();
        }
        // end of TIGHTENING 02
        ///////////////////////////////////////////////////////////////

        ///////////////////////////////////////////////////////////////
        // TIGHTENING 03
        if (runTightening03) {
            /* TIMING */timing.startTimer(timerStr + "-tightening03");
            tightening03();
            /* TIMING */timing.stopTimer(timerStr + "-tightening03");
            diaResult._timingTightening03 = timing.getLastElapsedTime();
        }
        // end of TIGHTENING 02
        ///////////////////////////////////////////////////////////////

        /* TIMING */timing.stopTimer(timerStr);
        diaResult._totalTiming = timing.getLastElapsedTime();

        double exponent = diaResult.getDiameter();
        if (DiameterResult.isTooLarge(exponent)) {
            return -exponent;
        } else {
            return Math.pow(2, exponent);
        }
    }

    /** Gathers the simple metrics from the input policy:
     * <ul>
     * <li>|Users| = |Roles|
     * <li>|Roles|
     * <li>|Timeslots|
     * </ul>
     * Also sets the initial diameter (before any tightenings are applied) */
    public void gatherSimpleMetrics() {
        diaResult._numRules = m.numberOfRules();
        diaResult._numRoles = m.numberOfRoles();
        diaResult._numTimeslots = m.numberOfTimeslots();

        diaResult._diameterInitial = ((diaResult._numRoles + 1) * diaResult._numRoles * diaResult._numTimeslots);
        diaResult._diameterInitialStr = "2^{((" + diaResult._numRoles + " + 1) * " + diaResult._numRoles + " * "
                + diaResult._numTimeslots + ")}";
    }

    /** Finds all of the admin roles and calculates the tightening01 diameter. */
    public void tightening01() {
        getAdminRoles();

        diaResult._diameterTightening01 = (((diaResult._numAdminRoles + 1) + 1) * diaResult._numRoles
                * diaResult._numTimeslots);
        diaResult._diameterTightening01Str = "2^{(((" + diaResult._numAdminRoles + " + 1) + 1) * " + diaResult._numRoles
                + " * " + diaResult._numTimeslots + ")}";
    }

    /** Gets the admin roles and sets the diameter results */
    private void getAdminRoles() {
        if (adminRoles == null) {
            // adminRoles = RoleHelper.getAdministratorRoles(m.getAllRules(), false);
            adminRoles = m.roleHelper.getAdminRoles();
            diaResult._numAdminRoles = adminRoles.size();
        }
    }

    /** Gets the goal roles and sets the diameter results */
    private void getGoalRoles() {
        if (goalRoles == null) {
            goalRoles = new HashSet<Role>(m.query._roles);
        }
    }

    /** Gets the CA Target Roles and sets the diameter results */
    private void getCATargetRoles() {
        if (CATargetRoles == null) {
            CATargetRoles = RoleHelper.getTargetRoles(m.canAssign.getRules(), false);
        }
    }

    /** Gets the CA/CR Positive Precondition Roles and sets the diameter results */
    private void getCACRPositivePrecondition() {
        if (CACRPositivePrecondition == null) {
            CACRPositivePrecondition = RoleHelper.getPositivePreconditionRoles(m.canAssign.getRules(), false);
            CACRPositivePrecondition.addAll(RoleHelper.getPositivePreconditionRoles(m.canRevoke.getRules(), false));
        }
    }

    /** Gets the CE Target Roles and sets the diameter results */
    private void getCETargetRoles() {
        if (CETargetRoles == null) {
            CETargetRoles = RoleHelper.getTargetRoles(m.canEnable.getRules(), false);

        }
    }

    /** Gets the CE/CD Positive Precondition Roles and sets the diameter results */
    private void getCECDPositivePrecondition() {
        if (CECDPositivePrecondition == null) {
            CECDPositivePrecondition = RoleHelper.getPositivePreconditionRoles(m.canEnable.getRules(), false);
            CECDPositivePrecondition.addAll(RoleHelper.getPositivePreconditionRoles(m.canDisable.getRules(), false));
        }
    }

    /** Finds all of the CA Target and CE Target roles. */
    public void tightening02() {
        getAdminRoles();
        getGoalRoles();
        getCATargetRoles();
        getCACRPositivePrecondition();

        Set<Role> userRoleSet = new HashSet<Role>(CATargetRoles.size());
        // ({CA-CR-Positive-Precondition} \cup {Admin-Roles} \cup {Goal-Roles})
        userRoleSet.addAll(CACRPositivePrecondition);
        userRoleSet.addAll(adminRoles);
        userRoleSet.addAll(goalRoles);
        // {CA-Target-Roles} \cap ({CA-CR-Positive-Precondition} \cup {Admin-Roles} \cup {Goal-Roles})
        userRoleSet.retainAll(CATargetRoles);

        getCETargetRoles();
        getCECDPositivePrecondition();

        Set<Role> roleEnablementSet = new HashSet<Role>(CETargetRoles.size());
        // ({CE-CD-Positive-Precondition} \cup {Admin-Roles})
        roleEnablementSet.addAll(CECDPositivePrecondition);
        roleEnablementSet.addAll(adminRoles);
        // {CE-Target-Roles} \cap ({CE-CD-Positive-Precondition} \cup {Admin-Roles})
        roleEnablementSet.retainAll(CETargetRoles);

        int numCATargetTimeslots = TimeIntervalHelper.getTargetTimeslots(m.canAssign.getRules(), false).size();
        int numCETargetTimeslots = TimeIntervalHelper.getTargetTimeslots(m.canEnable.getRules(), false).size();

        diaResult._diameterTightening02 = ((diaResult._numAdminRoles + 1) * userRoleSet.size() * numCATargetTimeslots
                + roleEnablementSet.size() * numCETargetTimeslots);
        diaResult._diameterTightening02Str = "2^{((" + diaResult._numAdminRoles + " + 1) * " + userRoleSet.size()
                + " * " + numCATargetTimeslots + " + " + roleEnablementSet.size() + " * " + numCETargetTimeslots + ")}";
    }

    /** Find all Longest-Shortest Paths for the Roles in $G$ using the rules in $S_{pos}$ and $S_{neg}$
     * 
     * @param G
     *            Set of Roles to perform and combine Longest-Shortest Path algorithm
     * @param S_Pos
     *            Set/List of Rules
     * @param S_Neg
     *            Set/List of Rules
     * @return */
    public SPResult SP(Set<Role> G, ArrayList<Rule> S_Pos, ArrayList<Rule> S_Neg) {
        SPResult result = new SPResult();

        for (Role g : G) {
            SPResult r = FindLongestShortestPath(g, S_Pos, S_Neg);
            result.R.addAll(r.R);
            result.T.addAll(r.T);
        }

        return result;
    }

    public SPResult SP(Role g, ArrayList<Rule> S_Pos, ArrayList<Rule> S_Neg) {
        Set<Role> G = new HashSet<>(1);
        G.add(g);
        return SP(G, S_Pos, S_Neg);
    }

    public SPResult FindLongestShortestPath(Role r, ArrayList<Rule> S_Pos, ArrayList<Rule> S_neg) {
        SPResult result = new SPResult();

        ArrayList<Rule> A = RulesWithTargetRole(r, S_Pos);

        for (Rule a : A) {
            SPResult res = ShortestPath(a, S_Pos, S_neg);

            if (res.R.size() > result.R.size()) {
                logger.fine("Longer Shortest Path:" + res + "; Old Path: " + result);
                result = res;
            }
        }

        return result;
    }

    private SPResult ShortestPath(Rule a, ArrayList<Rule> S_Pos, ArrayList<Rule> S_Neg) {
        SPResult result = new SPResult();

        Set<Role> C_Pos = new HashSet<>();
        Set<Role> C_Neg = new HashSet<>();
        Set<Role> R_Pos = new HashSet<>();
        Set<Role> R_NegPos = new HashSet<>();
        Stack<Rule> queue = new Stack<>();
        queue.push(a);

        while (queue.isEmpty() == false) {
            Rule b = queue.pop();
            result.R.add(b._role);
            C_Neg.addAll(b.getNegativePreconditionRoles());

            if (b._type == RuleType.ASSIGN || b._type == RuleType.ENABLE) {
                result.T.addAll(b._roleSchedule);
            }

            // R_Pos := (b.getPositivePreconditionRoles() - C_Pos) - R
            R_Pos = new HashSet<>(b.getPositivePreconditionRoles());
            R_Pos.removeAll(C_Pos);
            R_Pos.removeAll(result.R);
            C_Pos.addAll(R_Pos);

            for (Role p : R_Pos) {
                queue.addAll(RulesWithTargetRole(p, S_Pos));
            }

            if (queue.isEmpty()) {
                // R_NegPos := (C_Neg \cap C_Pos) - result.R;
                R_NegPos = new HashSet<>(C_Neg);
                R_NegPos.retainAll(C_Pos);
                R_NegPos.removeAll(result.R);

                for (Role np : R_NegPos) {
                    queue.addAll(RulesWithTargetRole(np, S_Neg));
                }
            }
        }

        return result;
    }

    /** Simple function to grab all rules with a specific target role {@code r}.
     * 
     * @param r
     * @param S
     * @return */
    private ArrayList<Rule> RulesWithTargetRole(Role r, ArrayList<Rule> S) {
        ArrayList<Rule> rules = new ArrayList<>();

        for (Rule s : S) {
            if (s._role.equals(r)) {
                rules.add(s);
            }
        }

        return rules;
    }

    /** Set the CARules */
    private void getCARules() {
        if (CARules == null) {
            CARules = m.canAssign.getRules();
        }
    }
    /** Set the CRRules */
    private void getCRRules() {
        if (CRRules == null) {
            CRRules = m.canRevoke.getRules();
        }
    }
    /** Set the CERules */
    private void getCERules() {
        if (CERules == null) {
            CERules = m.canEnable.getRules();
        }
    }
    /** Set the CDRules */
    private void getCDRules() {
        if (CDRules == null) {
            CDRules = m.canDisable.getRules();
        }
    }

    public void tightening03() {
        getCARules();
        getCRRules();
        getCERules();
        getCDRules();
        getAdminRoles();
        getGoalRoles();
        getCATargetRoles();
        getCETargetRoles();

        SPResult target = SP(goalRoles, CARules, CRRules);

        Set<Role> target_CAcapR = new HashSet<>(CATargetRoles);
        target_CAcapR.retainAll(target.R);
        int U_goal = target_CAcapR.size() * target.T.size();

        if (DEBUG) {
            logger.fine("target.R = " + target.R);
            logger.fine("target.T = " + target.T);
            logger.fine("target_CAcapR = " + target_CAcapR);
        }

        int U_admins = 0;
        Set<Role> tmp_r = new HashSet<>();
        Set<TimeSlot> tmp_ts = new HashSet<>();
        StringBuilder sb = new StringBuilder();
        int i = 0;

        for (Role a : adminRoles) {
            SPResult adminResult = SP(a, CARules, CRRules);

            if (DEBUG) {
                logger.fine("adminResult." + i + ".R = " + adminResult.R);
                logger.fine("adminResult." + i + ".T = " + adminResult.T);
            }

            adminResult.R.retainAll(CATargetRoles);
            if (i > 0) {
                sb.append(" + ");
            }
            sb.append(adminResult.R.size()).append(" * ").append(adminResult.T.size());

            U_admins += adminResult.R.size() * adminResult.T.size();

            ////////////////////////////////////////////////////////////////////////
            // RE Calculations
            SPResult reResult = SP(a, CERules, CDRules);
            if (DEBUG) {
                logger.fine("reResult." + i + ".R = " + reResult.R);
                logger.fine("reResult." + i + ".T = " + reResult.T);
            }

            tmp_r.addAll(reResult.R);
            tmp_ts.addAll(reResult.T);
            ////////////////////////////////////////////////////////////////////////

            i++;
        }

        if (sb.length() == 0) {
            sb.append("0");
        }

        tmp_r.retainAll(CETargetRoles);
        int RE = tmp_r.size() * tmp_ts.size();

        if (DEBUG) {
            logger.fine("tmp_r = " + tmp_r);
            logger.fine("tmp_ts = " + tmp_ts);
        }

        diaResult._diameterTightening03 = U_goal + U_admins + RE;
        diaResult._diameterTightening03Str = "2^{(" + target_CAcapR.size() + " * " + target.T.size() + ") + (" //
                + sb.toString() + ") + (" //
                + tmp_r.size() + " * " + tmp_ts.size() + ")}";
    }
}
