package cree.optimization.boundestimation;

import java.util.HashSet;
import java.util.Set;

import mohawk.global.pieces.Role;
import mohawk.global.pieces.TimeSlot;

public class SPResult {
    public Set<Role> R = new HashSet<>();
    public Set<TimeSlot> T = new HashSet<>();

    @Override
    public String toString() {
        return "SPResult {R:" + R + ", T:" + T + "}";
    }
}
